# OGG2MOGG by Michael Tolly (Onyxite/mtolly)

This program is taken from [this GitHub release](https://github.com/mtolly/ogg2mogg/releases/tag/20191108), and is included as part of the Rock Band Customs Project. It builds the correct MOGG files for Harmonix games using `libvorbisfile`, unlike MahoppianGoon's tool which uses a corrupt header.

Original credits:
>>>
Written by Michael Tolly (onyxite), built on research by xorloser and maxton.
Code is in the public domain.
>>>

To use, type this in a Command Prompt:
```
ogg2mogg.exe in.ogg out.mogg
```
...Where `in.ogg` is your OGG file, and `out.MOGG` is the MOGG you need for the Rock Band custom DLC. This works with song MOGGs as well as preview MOGGs.

# TO-DO

* Create a Mac/Linux-capable version, using Python or similar.
* Create a GUI version.