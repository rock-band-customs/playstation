# :guitar: THE ROCK BAND CUSTOMS PROJECT 

This GitLab repository is part of the **Rock Band Customs Project** founded by Harvey Houston (HarvHouHacker). It is a collection of software designed to be used for the consoles mentioned in the project title. For info about proper usage, copyright, and other information, please see the [Terms and Conditions](https://gitlab.com/-/snippets/2068521) before using ANY of the software provided here.

# :video_game: ROCK BAND CUSTOMS FOR PLAYSTATION 3

Since the delisting of Rock Band DLC on the Playstation Store, it is impossible to get more songs from the Music Stores of certain Rock Band titles. Thus, the files included in this repo will allow you to add new songs to those titles via an unconventional hacking method.

## :exclamation: A Word of Warning

Any modifications may brick your Playstation 3. This means that it may make your PS3 unusable. I strongly recommend backing up saves and games to an USB drive, and for those who may be a bit ignorant (it's okay! We all were once), any critical system files as well. If you do not want to mod, stop reading!

# :book: PLEASE READ THE WIKI!

Newcomers to Rock Band hacking may find this software confusing if they don't know how to use it. I recommend a careful and thorough reading of the **Basic Guide to Adding Customs** section of the [Rock Band Customs GitLab Wiki](https://gitlab.com/rock-band-customs/playstation/-/wikis/home) to get you started.

# :file_folder: REPO CONTENTS

For PC and homebrew optimized for both OFW and HFW, download the entire `master` branch, either by using `git`, or by downloading a compressed package (ZIP, TAR, or whatever) directly from here on GitLab.

## :desktop: For Windows

A minimum version of Windows 7 is recommended to run this software, although this software can be used on versions as low as XP 64-bit. For Linux users, WINE **may** work. Anyone who tests these in WINE is welcome to report their findings, and create new issues if they run across problems.

* **Xorloser's ArkTools:** Tools which extract and modify archives on Rock Band game discs. **Do not share extracted archives!** 
* **MahoppianGoon's Rock Band DLC Tools:**  Made mainly for Rock Band customs on the Xbox, it also has some very useful tools for the PS3 versions. The tools include:
    - *OGG2MOGG:* This will convert multitrack OGG audio files into Rock Band compatible files with the MOGG extension.
    - *Songs.dta Editor:* This will aid in editing DTA files so that they can be read by Rock Band titles.
    - The other tools are mainly for Xbox users, but you are welcome to mess aroud with 'em anyway!'

## :link: Links to Additional Software

These are links to other software that will be very useful for making Rock Band custom DLC. *Software is mentioned in the wiki.*

# :thumbsup: SPECIAL THANKS

* **pksage** - The guy who pretty much conceived Rock Band customs to begin with, he created Customs Creators (which is now Rhythm Gaming World). He is, of course, on [Rhythm Gaming World](https://rhythmgamingworld.com/members/pksage/).
* **StackOverflow0x/Koetsu** - This guy is the one who inspired me to go all-out on hacking Rock Band 3. You can check out his profiles on [Rhythm Gaming World (as StackOverflow0x)](https://rhythmgamingworld.com/members/stackoverflow0x/) and [ScoreHero (as Koetsu)](https://rockband.scorehero.com/scores.php?user=484319&diff=).
* **Alternity** - He helped me understand how to use copyright material responsibly, and his suggestions on improving RBC Discord made it all the more manageable. He submitted various Rock Band tools to Discord, which have helped a great deal. He is on [Rhythm Gaming World](https://rhythmgamingworld.com/members/alternity/).
* **Farottone** - One of the administrators of Rhythm Gaming World, he helped me with understanding about copyrighted content, and is kind of like a Rock Band sensei to me. You can see him on [Rhythm Gaming World](https://rhythmgamingworld.com/members/farottone/).
* **TrashRBPlayer** - Another person who really helped me out on RBC Discord, he suggested expanding the Rock Band Customs project to more than just for Wii/vWii. Find him on [Rhythm Gaming World](https://rhythmgamingworld.com/members/trashrbplayer).
* **MahoppianGoon** - His software and guides are mainly for Xbox 360, but his OGG2MOGG and songs.dta Editor Tools help a lot for PS3 customs! Visit his site on [Google Sites](https://sites.google.com/site/mahoppiangoon/home).
* **Xorloser** - He created the Arktools suite which can extract ARK packages found in Rock Band discs, as well as convert songs from Gutar Hero to be used in Rock Band, among other things which I have yet to figure out. Find his blog on [WordPress](http://www.xorloser.com).
* **Ethan Gordon (Miramur)** - With an interest in robotics and programming, he jumped right into the project, and has helped enhance the wiki as well as filling in for me on Discord. Find him on [his website](https://ethankgordon.com), [GitLab](https://gitlab.com/ekgordon), and [GitHub](https://github.com/egordon).
